# About

This is a client demo project.
Connecting to an implementation of this tutorial:

[`https://spring.io/guides/gs/messaging-stomp-websocket/`](https://spring.io/guides/gs/messaging-stomp-websocket/)

A few things are customized.

# Configuring your ENV:

Make sure you have the following installed:

- NodeJS
- NPM

Obtain the `node_modules` folder by executing:

    $ npm install

# Execution

First time execution (or first time after 24hrs):

    $ USR=<USERNAME> PASS=<PASSWORD> node index.js

Aftwards:

    $ node index.js

## Making it even easier

Make it executable if it already isn't:

    $ chmod u+x index.js

Then all you need to do is:

    $ ./index.js

# Demo Controller

```java
package br.com.neppo.ian.websocket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DemoController {

    @MessageMapping("/testing")
    @SendTo("/heya/boi")
    public static Map<String, Object> heya(Map<String, Object> ayy){
        Map<String, Object> hello = new HashMap<>();
        hello.put("message", "kay");
        return hello;
    }

}
```