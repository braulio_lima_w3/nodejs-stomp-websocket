const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

const config = {}
const host = 'http://localhost:8080/api/websocket';

var stompClient = null;

var tasksAfterConnect = () => console.log('nothing to do') || disconnect();

function connect() {
    var socket = new SockJS(host);
    stompClient = Stomp.over(socket);
    stompClient.connect(config, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/heya/boi', function (greeting) {
            console.log(greeting.body);
        });

        tasksAfterConnect();
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function setConnected(val){
    // do nothing....
}

module.exports = function(authToken){

    config.headers = { Authorization: authToken }

    tasksAfterConnect = () => {
        if(!stompClient) return;
        stompClient.send('/api/testing', {}, JSON.stringify({ test : 'test msg'}));
        // disconnect();
    }

    connect();

}