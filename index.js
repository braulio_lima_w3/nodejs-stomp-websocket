#!node

const auth = require('./authentication');
const socket = require('./socket');
const fs = require('fs');
const event = require('events');

var username = process.env.USR || '';
var password = process.env.PASS || '';

const encoding = 'utf8';
var location = './.authtoken';
var file = null;


if(fs.existsSync(location)){
    file = fs.readFileSync(location, { encoding : encoding });
}

function saveToken(token){
    if(!token) return;
    var auth = 'bearer ' + token;
    fs.writeFileSync(location, auth, {encoding : encoding});
    file = auth;
    return true;
}

function websocket(){
    socket(file);
    return true;
}

if(file == null){
    auth(username, password)
        .then(data => saveToken(data.accessToken),  websocket())
        .catch(err => console.log('nothing to do'));
}
else {
    websocket(file);
}

