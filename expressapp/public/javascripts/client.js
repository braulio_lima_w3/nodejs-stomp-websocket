var stompClient = null;

const endpoint = 'http://localhost:8080/conversation-api/websocket';

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS(endpoint);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendData() {
    stompClient.send(getEndpoint(), {}, $("#name").val());
}

function getEndpoint(){
    return $("#endpoint").val() || '/testing';
}

function displayData(message) {
    $("#responses").html(`AT: ${new Date()}\n${message}\n\n----\n\n${$("#responses").html()}`)
}

$(function () {
    $(".websocket").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendData(); });
    $( "#setListen" ).click(function() { addListener(); });
    
});

registered = {}
function register(route, cb){

    if(!stompClient || !route || registered[route]){
        return;
    }
    registered[route] = true;
    stompClient.subscribe(route, function (data) {
        displayData(data);
        if(cb){
            return cb(data);
        }
    });
    updateListened();
    $("#listen").val('');
}

function updateListened(){
    var keys = Object.keys(registered);
    var output = '';
    keys.forEach(key => {
        if(output){
            output += ', ';
        }
        output += key;
    })
    $("#listListened").html(output);
}

function addListener(){
    register($("#listen").val());
}