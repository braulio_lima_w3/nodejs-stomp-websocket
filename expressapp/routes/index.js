var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.cookies);
  
  res.render('index', { title: 'Express', authenticated : 'AUTH-TOKEN' in req.cookies && 'c_user' in req.cookies, authtoken : req.cookies['AUTH-TOKEN'] });
});

router.get('/clear', function(req, res, next){
  res.clearCookie('AUTH-TOKEN')
  res.clearCookie('c_user')
  res.redirect('/')
})

module.exports = router;
