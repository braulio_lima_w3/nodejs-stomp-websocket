var express = require('express');
var axios = require('axios');
var router = express.Router();

/* GET users listing. */
router.post('/login', (req, res, next) => {

  console.log(req.body);
  

  axios.post('http://localhost:8080/login', `username=${req.body.username}&password=${req.body.password}`, {
      headers : {
          'Content-Type' : 'application/x-www-form-urlencoded'
      }
  }).then(data=> {
    console.log(data.body)
    Object.keys(data.headers).forEach(key => {
      res.setHeader(key, data.headers[key]);
    });
    res.status(300).redirect('/');
  }).catch(err => {
    next(new Error('Could not login'));
  });
})

module.exports = router;
