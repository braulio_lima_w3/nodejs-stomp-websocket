const axios = require("axios");

module.exports = function(username, password){
    return new Promise((ya, no)=>{
        if(!username || !password){
            return console.log('Username and or password is empty') || no(null);
        }
        axios.post(
            'http://localhost:8080/api/login',
            `username=${username}&password=${password}`,
            {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded'
                }
            }
        ).then(a => console.log('Logged in') || ya(a.data))
        .catch(err => console.log('Could not login. Reason:', err && err.response && err.response.status || 0) || no(err.response));
    });
}